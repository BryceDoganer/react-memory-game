import React, { Component } from 'react';
import NavBar from './NavBar'
import CardGrid from './CardGrid';
import './App.css';

const numPairs = 10;

class App extends Component {
  constructor(props) {
    super(props);
    const initialCards = this.populateMemoryCards(numPairs);
    this.state = {
      cards: initialCards,
      revealedCard: -1
    }
    
    this.handleClick = this.handleClick.bind(this);
    this.resetGame = this.resetGame.bind(this);
    this.hideCards = this.hideCards.bind(this);
  }
  
  populateMemoryCards(numPairs) {
    let {colors} = this.props;
    let cards = [];
    for(let i = 0; i < numPairs; i++) {
      const colorIndex = Math.floor(Math.random() * colors.length);
      const card = {
        color: colors[colorIndex],
        shown: false
      }
      cards = [...cards, card, card];
      colors.splice(colorIndex, 1);
    }
    return cards.sort(() => Math.random() - 0.5);
  }
  
  hideCards(...ids) {
    console.log(`Hiding Ids: ${ids}`);
    const {cards} = this.state; 
    console.log(cards);
    const updatedCards = cards.map((card, index) => {
      if(ids.includes(index)) {
        return {...card, shown: false}
      } else {
        return card; 
      }
    });
    this.setState({cards: updatedCards});
  }
  
  handleClick(id) {
    const {cards, revealedCard} = this.state; 
    if(cards[id].shown) return;
    
    const newCards = cards.map((card, index) => {
      if(index === id) {
        return {...card, shown: true}
      } else {
        return card; 
      }
    });
    
    this.setState({cards: newCards});
    
    if(revealedCard >= 0) {
      if(id !== revealedCard) {
        if(cards[id].color !== cards[revealedCard].color) {
          setTimeout(() => {
            this.hideCards(revealedCard, id);
            console.log("Timeout");
          }, 1000);
        }
        this.setState({revealedCard: -1});
      }
    } else {
        this.setState({revealedCard: id});
    }
  }
  
  resetGame() {
    const newCards = this.populateMemoryCards(numPairs);
    this.setState({cards: newCards});
  }
  
  render() {
    const cards = this.state.cards; 
    return (
      <div>
        <NavBar reset={this.resetGame} />
        <CardGrid cards={cards} handleClick={this.handleClick}/>
      </div>
    );
  }
}

App.defaultProps = {
  colors: ["AliceBlue","AntiqueWhite","Aqua","Aquamarine","Azure","Beige","Bisque","Black","BlanchedAlmond",
              "Blue","BlueViolet","Brown","BurlyWood","CadetBlue","Chartreuse","Chocolate",
              "Coral","CornflowerBlue","Cornsilk","Crimson","Cyan","DarkBlue","DarkCyan","DarkGoldenRod",
              "DarkGray","DarkGrey","DarkGreen","DarkKhaki","DarkMagenta","DarkOliveGreen","Darkorange",
              "DarkOrchid","DarkRed","DarkSalmon","DarkSeaGreen","DarkSlateBlue","DarkSlateGray","DarkSlateGrey",
              "DarkTurquoise","DarkViolet","DeepPink","DeepSkyBlue","DimGray","DimGrey","DodgerBlue",
              "FireBrick","FloralWhite","ForestGreen","Fuchsia","Gainsboro","GhostWhite","Gold","GoldenRod",
              "Gray","Grey","Green","GreenYellow","HoneyDew","HotPink","IndianRed","Indigo","Ivory","Khaki",
              "Lavender","LavenderBlush","LawnGreen","LemonChiffon","LightBlue","LightCoral","LightCyan",
              "LightGoldenRodYellow","LightGray","LightGrey","LightGreen","LightPink","LightSalmon",
              "LightSeaGreen","LightSkyBlue","LightSlateGray","LightSlateGrey","LightSteelBlue","LightYellow",
              "Lime","LimeGreen","Linen","Magenta","Maroon","MediumAquaMarine","MediumBlue","MediumOrchid",
              "MediumPurple","MediumSeaGreen","MediumSlateBlue","MediumSpringGreen","MediumTurquoise",
              "MediumVioletRed","MidnightBlue","MintCream","MistyRose","Moccasin","NavajoWhite","Navy",
              "OldLace","Olive","OliveDrab","Orange","OrangeRed","Orchid","PaleGoldenRod","PaleGreen",
              "PaleTurquoise","PaleVioletRed","PapayaWhip","PeachPuff","Peru","Pink","Plum","PowderBlue",
              "Purple","Red","RosyBrown","RoyalBlue","SaddleBrown","Salmon","SandyBrown","SeaGreen",
              "SeaShell","Sienna","Silver","SkyBlue","SlateBlue","SlateGray","SlateGrey","Snow","SpringGreen",
              "SteelBlue","Tan","Teal","Thistle","Tomato","Turquoise","Violet","Wheat","White","WhiteSmoke",
              "Yellow","YellowGreen"]
};

export default App;
