import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from './Card';
import './CardGrid.css'


class CardGrid extends Component {
  
  render() {
    const {cards, handleClick} = this.props;
    const cardComponents = cards.map((card, index) => 
      <Card handleClick={handleClick} 
            color={card.color} 
            shown={card.shown} 
            id={index} 
            key={index} 
      />
    );
    return(
      <div className="card-grid">
        {cardComponents}
      </div>
    );
  } 
}


export default CardGrid;