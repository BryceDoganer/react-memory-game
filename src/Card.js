import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Card.css'


class Card extends Component {
  
  render() {
    const {color, id, shown, handleClick} = this.props;
    let backgroundColor = shown ? color : "grey";
    // console.log(`div ${id}. Color: ${backgroundColor}`);
  
    return (
      <div className="card" 
           onClick={() => handleClick(id)} 
           style={{background: backgroundColor}}
      />
    );
  } 
}

export default Card;