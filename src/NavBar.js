import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Menu } from 'semantic-ui-react';
// import './NavBar.css';

class NavBar extends Component {
  state = {}

  handleItemClick = (e, { name }) => {
    this.setState({ activeItem: name });
    this.props.newGame(); 
  };

  render() {
    const { activeItem } = this.state;
    const { newGame, reset } = this.props;

    return (
      <Menu inverted stackable>
        <Menu.Item>
          Memory Game
        </Menu.Item>
      
        <Menu.Menu position='right'>
          <Menu.Item name='new' onClick={reset}>
            New Game
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    )
  }
}

NavBar.defaultProps = {
  onNewRecipe() {}
}

NavBar.propTypes = {
  onNewRecipe: PropTypes.func
}


export default NavBar;

